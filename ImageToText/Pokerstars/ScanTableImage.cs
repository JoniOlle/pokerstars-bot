﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageToText.Pokerstars
{
    class ScanTableImage
    {
        public static Bitmap CropTableName(Bitmap windowImage)
        {
            return windowImage.Clone(new Rectangle(28, 10, 353, 17), windowImage.PixelFormat);
        }

        private static Bitmap CropTablePotSize(Bitmap windowImage)
        {
            Bitmap potSize = windowImage.Clone(new Rectangle(210, 33, 76, 21), windowImage.PixelFormat);

            Color[][] pixels = ImageTools.ProcessImage(potSize);

            Color bg = Color.FromArgb(243, 234, 194);

            Rectangle textArea = new Rectangle();

            // loop
            int width = pixels[0].Length;
            int height = pixels.Length;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (ImageTools.WithinTolerance(bg, pixels[y][x], 10))
                    {
                        if (textArea.X == 0)
                        {
                            textArea.X = x;
                            textArea.Y = y;
                        }
                        else
                        {
                            textArea.Width = x - textArea.X;
                            textArea.Height = y - textArea.Y + 1;
                        }
                    }
                }
            }

            if (textArea.Width > 0 && textArea.Height > 0)
                potSize = potSize.Clone(textArea, potSize.PixelFormat);

            return potSize;
        }

        public static float GetPotSize(Bitmap windowImage)
        {
            Bitmap potsizePicture = CropTablePotSize(windowImage);

            Color[][] pixels = ImageTools.ProcessImage(potsizePicture);

            int width = pixels[0].Length;
            int height = pixels.Length;

            Color black = Color.FromArgb(0, 0, 0);

            string numbers = "";

            // loop trough every pixel
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    // x = 4, y = 7

                    // 0
                    if (pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] == black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] == black &&

                        pixels[y + 3][x + 0] == black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] != black &&
                        pixels[y + 3][x + 3] == black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] == black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] == black &&

                        pixels[y + 6][x + 0] != black &&
                        pixels[y + 6][x + 1] == black &&
                        pixels[y + 6][x + 2] == black &&
                        pixels[y + 6][x + 3] != black)
                        numbers += "0";

                    else if ( // 1
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] != black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] != black &&
                        pixels[y + 1][x + 1] == black &&
                        pixels[y + 1][x + 2] == black &&
                        pixels[y + 1][x + 3] != black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] == black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] != black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] == black &&
                        pixels[y + 4][x + 3] != black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] == black &&
                        pixels[y + 5][x + 3] != black &&

                        pixels[y + 6][x + 0] != black &&
                        pixels[y + 6][x + 1] == black &&
                        pixels[y + 6][x + 2] == black &&
                        pixels[y + 6][x + 3] == black)
                        numbers += "1";

                    else if ( // 2
                        pixels[y + 0][x + 0] == black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] != black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] == black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] != black &&
                        pixels[y + 4][x + 1] == black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] != black &&

                        pixels[y + 5][x + 0] == black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] != black &&

                        pixels[y + 6][x + 0] == black &&
                        pixels[y + 6][x + 1] == black &&
                        pixels[y + 6][x + 2] == black &&
                        pixels[y + 6][x + 3] == black)
                        numbers += "2";

                    else if ( // 3
                        pixels[y + 0][x + 0] == black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] != black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] == black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] == black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] != black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] == black &&

                        pixels[y + 6][x + 0] == black &&
                        pixels[y + 6][x + 1] == black &&
                        pixels[y + 6][x + 2] == black &&
                        pixels[y + 6][x + 3] != black)
                        numbers += "3";

                    else if ( // 4
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] != black &&
                        pixels[y + 0][x + 2] != black &&
                        pixels[y + 0][x + 3] == black &&

                        pixels[y + 1][x + 0] != black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] == black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] == black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] == black &&

                        pixels[y + 3][x + 0] == black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] != black &&
                        pixels[y + 3][x + 3] == black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] == black &&
                        pixels[y + 4][x + 2] == black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] == black &&

                        pixels[y + 6][x + 0] != black &&
                        pixels[y + 6][x + 1] != black &&
                        pixels[y + 6][x + 2] != black &&
                        pixels[y + 6][x + 3] == black)
                        numbers += "4";

                    else if ( // 5
                        pixels[y + 0][x + 0] == black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] == black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] != black &&

                        pixels[y + 2][x + 0] == black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] == black &&
                        pixels[y + 3][x + 1] == black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] != black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] == black &&

                        pixels[y + 6][x + 0] == black &&
                        pixels[y + 6][x + 1] == black &&
                        pixels[y + 6][x + 2] == black &&
                        pixels[y + 6][x + 3] != black)
                        numbers += "5";

                    else if ( // 6
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] != black &&

                        pixels[y + 2][x + 0] == black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] == black &&
                        pixels[y + 3][x + 1] == black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] == black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] == black &&

                        pixels[y + 6][x + 0] != black &&
                        pixels[y + 6][x + 1] == black &&
                        pixels[y + 6][x + 2] == black &&
                        pixels[y + 6][x + 3] != black)
                        numbers += "6";

                    else if ( // 7
                        pixels[y + 0][x + 0] == black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] == black &&

                        pixels[y + 1][x + 0] != black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] == black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] != black &&
                        pixels[y + 4][x + 1] == black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] != black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] == black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] != black &&

                        pixels[y + 6][x + 0] == black &&
                        pixels[y + 6][x + 1] != black &&
                        pixels[y + 6][x + 2] != black &&
                        pixels[y + 6][x + 3] != black)
                        numbers += "7";

                    else if ( // 8
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] == black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] == black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] == black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] == black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] == black &&

                        pixels[y + 6][x + 0] != black &&
                        pixels[y + 6][x + 1] == black &&
                        pixels[y + 6][x + 2] == black &&
                        pixels[y + 6][x + 3] != black)
                        numbers += "8";

                    else if ( // 9
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] == black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] == black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] == black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] == black &&

                        pixels[y + 4][x + 0] != black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] == black &&

                        pixels[y + 6][x + 0] != black &&
                        pixels[y + 6][x + 1] == black &&
                        pixels[y + 6][x + 2] == black &&
                        pixels[y + 6][x + 3] != black)
                        numbers += "9";

                    else
                    {
                        if (numbers.Length > 0)
                        {
                            if (
                                pixels[y + 0][x + 0] != black &&
                                pixels[y + 0][x + 1] != black &&
                                pixels[y + 0][x + 2] != black &&
                                pixels[y + 0][x + 3] != black &&

                                pixels[y + 1][x + 0] != black &&
                                pixels[y + 1][x + 1] != black &&
                                pixels[y + 1][x + 2] != black &&
                                pixels[y + 1][x + 3] != black &&

                                pixels[y + 2][x + 0] != black &&
                                pixels[y + 2][x + 1] != black &&
                                pixels[y + 2][x + 2] != black &&
                                pixels[y + 2][x + 3] != black &&

                                pixels[y + 3][x + 0] != black &&
                                pixels[y + 3][x + 1] != black &&
                                pixels[y + 3][x + 2] != black &&
                                pixels[y + 3][x + 3] != black &&

                                //pixels[y + 4][x + 0] != black &&
                                pixels[y + 4][x + 1] != black &&
                                pixels[y + 4][x + 2] != black &&
                                pixels[y + 4][x + 3] != black &&

                                pixels[y + 5][x + 0] != black &&
                                pixels[y + 5][x + 1] != black &&
                                pixels[y + 5][x + 2] == black &&
                                pixels[y + 5][x + 3] != black &&

                                pixels[y + 6][x + 0] != black &&
                                pixels[y + 6][x + 1] != black &&
                                pixels[y + 6][x + 2] == black &&
                                pixels[y + 6][x + 3] != black)
                                numbers += ",";
                        }
                    }

                    if (x + 4 >= width)
                        break;
                }

                if (numbers.Length > 0) // numbers in single line
                    break;
            }

            float pSize;
            if (float.TryParse(numbers, out pSize))
                return float.Parse(numbers);

            return 0;
        }


        // dealt hand id

        public static long CropTableHandId(Bitmap windowImage)
        {
            Bitmap handId = windowImage.Clone(new Rectangle(37, 31, 75, 12), windowImage.PixelFormat);

            Color[][] pixels = ImageTools.ProcessImage(handId);

            int width = pixels[0].Length;
            int height = pixels.Length;

            Color black = Color.FromArgb(16, 16, 16);

            string numbers = "";

            // loop trough every pixel
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    // x = 4, y = 6

                    // 0
                    if (pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] == black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] == black &&

                        pixels[y + 3][x + 0] == black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] != black &&
                        pixels[y + 3][x + 3] == black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] == black &&
                        pixels[y + 5][x + 2] == black &&
                        pixels[y + 5][x + 3] != black)
                        numbers += "0";

                    else if ( // 1
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] != black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] != black &&
                        pixels[y + 1][x + 1] == black &&
                        pixels[y + 1][x + 2] == black &&
                        pixels[y + 1][x + 3] != black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] == black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] != black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] == black &&
                        pixels[y + 4][x + 3] != black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] == black &&
                        pixels[y + 5][x + 3] != black)
                        numbers += "1";

                    else if ( // 2
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] == black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] == black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] != black &&

                        pixels[y + 5][x + 0] == black &&
                        pixels[y + 5][x + 1] == black &&
                        pixels[y + 5][x + 2] == black &&
                        pixels[y + 5][x + 3] == black)
                        numbers += "2";

                    else if ( // 3
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] != black &&
                        pixels[y + 2][x + 2] == black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] != black &&
                        pixels[y + 3][x + 3] == black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] == black &&
                        pixels[y + 5][x + 2] == black &&
                        pixels[y + 5][x + 3] != black)
                        numbers += "3";

                    else if ( // 4
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] != black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] != black &&
                        pixels[y + 1][x + 1] == black &&
                        pixels[y + 1][x + 2] == black &&
                        pixels[y + 1][x + 3] != black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] == black &&
                        pixels[y + 2][x + 2] == black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] == black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] == black &&
                        pixels[y + 4][x + 2] == black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] != black &&
                        pixels[y + 5][x + 2] == black &&
                        pixels[y + 5][x + 3] != black)
                        numbers += "4";

                    else if ( // 5
                        pixels[y + 0][x + 0] == black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] != black &&

                        pixels[y + 2][x + 0] == black &&
                        pixels[y + 2][x + 1] == black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] == black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] != black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] == black &&
                        pixels[y + 4][x + 3] != black &&

                        pixels[y + 5][x + 0] == black &&
                        pixels[y + 5][x + 1] == black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] != black)
                        numbers += "5";

                    else if ( // 6
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] != black &&

                        pixels[y + 2][x + 0] == black &&
                        pixels[y + 2][x + 1] == black &&
                        pixels[y + 2][x + 2] == black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] == black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] != black &&
                        pixels[y + 3][x + 3] == black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] == black &&
                        pixels[y + 5][x + 2] == black &&
                        pixels[y + 5][x + 3] != black)
                        numbers += "6";

                    else if ( // 7
                        pixels[y + 0][x + 0] == black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] != black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] == black &&
                        pixels[y + 1][x + 3] != black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] == black &&
                        pixels[y + 2][x + 2] != black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] == black &&
                        pixels[y + 3][x + 2] != black &&
                        pixels[y + 3][x + 3] != black &&

                        pixels[y + 4][x + 0] != black &&
                        pixels[y + 4][x + 1] == black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] != black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] == black &&
                        pixels[y + 5][x + 2] != black &&
                        pixels[y + 5][x + 3] != black)
                        numbers += "7";

                    else if ( // 8
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] == black &&
                        pixels[y + 2][x + 2] == black &&
                        pixels[y + 2][x + 3] != black &&

                        pixels[y + 3][x + 0] == black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] != black &&
                        pixels[y + 3][x + 3] == black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] == black &&
                        pixels[y + 5][x + 2] == black &&
                        pixels[y + 5][x + 3] != black)
                        numbers += "8";

                    else if ( // 9
                        pixels[y + 0][x + 0] != black &&
                        pixels[y + 0][x + 1] == black &&
                        pixels[y + 0][x + 2] == black &&
                        pixels[y + 0][x + 3] != black &&

                        pixels[y + 1][x + 0] == black &&
                        pixels[y + 1][x + 1] != black &&
                        pixels[y + 1][x + 2] != black &&
                        pixels[y + 1][x + 3] == black &&

                        pixels[y + 2][x + 0] != black &&
                        pixels[y + 2][x + 1] == black &&
                        pixels[y + 2][x + 2] == black &&
                        pixels[y + 2][x + 3] == black &&

                        pixels[y + 3][x + 0] != black &&
                        pixels[y + 3][x + 1] != black &&
                        pixels[y + 3][x + 2] != black &&
                        pixels[y + 3][x + 3] == black &&

                        pixels[y + 4][x + 0] == black &&
                        pixels[y + 4][x + 1] != black &&
                        pixels[y + 4][x + 2] != black &&
                        pixels[y + 4][x + 3] == black &&

                        pixels[y + 5][x + 0] != black &&
                        pixels[y + 5][x + 1] == black &&
                        pixels[y + 5][x + 2] == black &&
                        pixels[y + 5][x + 3] != black)
                        numbers += "9";

                    if (x + 4 >= width)
                        break;

                }

                if (numbers.Length > 0) // numbers in single line
                    break;
            }

            long hId;
            if (long.TryParse(numbers, out hId))
                return long.Parse(numbers);

            return 0;
        }

        // crop table cards

        public static Bitmap[] CropTableCards(Bitmap windowImage)
        {
            List<Bitmap> cards = new List<Bitmap>();

            Bitmap tableCards = windowImage.Clone(new Rectangle(164, 116, 170, 53), windowImage.PixelFormat);

            Color[][] pixels = ImageTools.ProcessImage(tableCards);

            int width = pixels[0].Length;
            int height = pixels.Length;

            Color white = Color.FromArgb(255, 255, 255); // card color

            // loop trough every pixel
            for (int y = 0; y < height; y++)
            {
                int inLine = 0;

                for (int x = 0; x < width; x++)
                {
                    if (pixels[y][x] == white)
                    {
                        inLine++;
                    }
                    else
                    {
                        if (inLine > 26)
                        {
                            // whole card  ( 28, 36 )

                            // only number ( 14, 14 )

                            Bitmap card = tableCards.Clone(new Rectangle(x - 28, y, 14, 14), tableCards.PixelFormat);

                            // copy tableCard
                            cards.Add(card);
                        }

                        inLine = 0;
                    }
                }

            }

            return cards.ToArray();
        }

        public static Poker.SUIT DetermineSuit(Bitmap cardImage)
        {
            Color[][] pixels = ImageTools.ProcessImage(cardImage);

            int width = pixels[0].Length;
            int height = pixels.Length;

            Color white = Color.FromArgb(255,255,255);
            Color red = Color.FromArgb(200, 20, 20);
            Color green = Color.FromArgb(30, 130, 10);
            Color blue = Color.FromArgb(10, 10, 230);
            Color black = Color.FromArgb(0, 0, 0);

            // red, green, blue, black
            int[] counter = new int[] { 0, 0, 0, 0 }; // track colors

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color pixel = pixels[y][x];

                    if (pixel != white)
                    {
                        if (ImageTools.WithinTolerance(pixel, red, 10))
                            counter[0]++;
                        else if (ImageTools.WithinTolerance(pixel, green, 10))
                            counter[1]++;
                        else if (ImageTools.WithinTolerance(pixel, blue, 10))
                            counter[2]++;
                        else if (ImageTools.WithinTolerance(pixel, black, 10))
                            counter[3]++;
                        else
                        {
                            // color not in any range
                        }
                    }
                }
            }

            Poker.SUIT rSuit = Poker.SUIT.None;

            int maxIndex = counter.ToList().IndexOf(counter.Max());

            switch (maxIndex)
            {
                case 0:
                    rSuit = Poker.SUIT.Hearts;
                    break;

                case 1:
                    rSuit = Poker.SUIT.Clubs;
                    break;

                case 2:
                    rSuit = Poker.SUIT.Diamonds;
                    break;

                case 3:
                    rSuit = Poker.SUIT.Spades;
                    break;

                default:
                    rSuit = Poker.SUIT.None;
                    break;
            }

            return rSuit;
        }

        public static Poker.Card[] TesseractTableCards(Bitmap windowImage)
        {
            List<Poker.Card> cards = new List<Poker.Card>();

            Bitmap[] tableCards = CropTableCards(windowImage);

            foreach (var card in tableCards)
            {
                Poker.Card solvedCard = new Poker.Card(Poker.RANK.None, Poker.SUIT.None); // null

                // get card's suit
                Poker.SUIT suit = DetermineSuit(card);

                string cardString = OCR.GetCharacter(card, 1);

                if (cardString.Length > 0)
                {

                    char cardRank = cardString[0];

                    if (char.IsDigit(cardRank))
                    {
                        if (cardRank == '2')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Two, suit);
                        }
                        else if (cardRank == '3')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Three, suit);
                        }
                        else if (cardRank == '4')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Four, suit);
                        }
                        else if (cardRank == '5')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Five, suit);
                        }
                        else if (cardRank == '6')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Six, suit);
                        }
                        else if (cardRank == '7')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Seven, suit);
                        }
                        else if (cardRank == '8')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Eight, suit);
                        }
                        else if (cardRank == '9')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Nine, suit);
                        }
                    }
                    else // ten's are emptys.. tessract can't regoc..
                    {
                        if (cardRank == 'J')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Jack, suit);
                        }
                        else if (cardRank == 'Q')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Queen, suit);
                        }
                        else if (cardRank == 'K')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.King, suit);
                        }
                        else if (cardRank == 'A')
                        {
                            solvedCard = new Poker.Card(Poker.RANK.Ace, suit);
                        }
                        else
                        {        
                            throw new ApplicationException("BUG this needs fixing!!");
                        }
                    }
                }
                else
                {
                    // cardRank == T
                    solvedCard = new Poker.Card(Poker.RANK.Ten, suit);
                }

                cards.Add(solvedCard);
            }

            return cards.ToArray();
        }

        #region Hash dictionary...
        public static readonly Dictionary<string, Poker.Card> cardsImageHashTable = new Dictionary<string,Poker.Card>()
        {
            // clubs
            {"EA55ED4C319531AFE05DB54A70686C38", new Poker.Card(Poker.RANK.Ace, Poker.SUIT.Clubs)},
            {"6CDADE0B25C80A731AF5CCFFB72FA4F6", new Poker.Card(Poker.RANK.Two, Poker.SUIT.Clubs)},
            {"9CF0CA5406E551B72EDFC7AB78E4D99A", new Poker.Card(Poker.RANK.Three, Poker.SUIT.Clubs)},
            {"6797C1261F6C06C0CDF54CE7FDD27CB8", new Poker.Card(Poker.RANK.Four, Poker.SUIT.Clubs)},
            {"A2C6F6E3002A7D2D9BA145D0C240266A", new Poker.Card(Poker.RANK.Five, Poker.SUIT.Clubs)},
            {"464FD48C4DC75C7A8B54C8A3FF31C3DA", new Poker.Card(Poker.RANK.Six, Poker.SUIT.Clubs)},
            {"C7228AD76B66D87ABFA019DBC63E04C5", new Poker.Card(Poker.RANK.Seven, Poker.SUIT.Clubs)},
            {"1B81A85EF816A652A2A272878BA8AA84", new Poker.Card(Poker.RANK.Eight, Poker.SUIT.Clubs)},
            {"FA9E58C33F60D27F99A67DC8AD645589", new Poker.Card(Poker.RANK.Nine, Poker.SUIT.Clubs)},
            {"F6F03FE4461269072018B48A8BB67F24", new Poker.Card(Poker.RANK.Ten, Poker.SUIT.Clubs)},
            {"FBEC07D6C50791855C3444BC1AB1AF77", new Poker.Card(Poker.RANK.Jack, Poker.SUIT.Clubs)},
            {"5D60086DF0E93B05A52AC1460C0EE5F2", new Poker.Card(Poker.RANK.Queen, Poker.SUIT.Clubs)},
            {"B7B0E35742A58719E0EA5BD4C06BAE57", new Poker.Card(Poker.RANK.King, Poker.SUIT.Clubs)},

            // hearts
            {"7DC5A68461A46B15059A0B49EF3F15CE", new Poker.Card(Poker.RANK.Ace, Poker.SUIT.Hearts)},
            {"4E68A5B6AD32D835386B0FF2B3AA2CE1", new Poker.Card(Poker.RANK.Two, Poker.SUIT.Hearts)},
            {"F3E5C4349A61C0A5A8A5E50F61935C87", new Poker.Card(Poker.RANK.Three, Poker.SUIT.Hearts)},
            {"6FED71B40F15DC175569E221982375EA", new Poker.Card(Poker.RANK.Four, Poker.SUIT.Hearts)},
            {"5FCFB207FAC05F7BFBB5BAA46D79F314", new Poker.Card(Poker.RANK.Five, Poker.SUIT.Hearts)},
            {"56687570E71ECCA6E5CEE33FAE5DAA08", new Poker.Card(Poker.RANK.Six, Poker.SUIT.Hearts)},
            {"3E3D85240B4BF5A81FD64F4FE3AE1D04", new Poker.Card(Poker.RANK.Seven, Poker.SUIT.Hearts)},
            {"34021C0FE0171497F14949E76B948A9B", new Poker.Card(Poker.RANK.Eight, Poker.SUIT.Hearts)},
            {"96663B0DD8298EE16CD8C64E2A705F45", new Poker.Card(Poker.RANK.Nine, Poker.SUIT.Hearts)},
            {"530B140FAF2A17FEA559910607795561", new Poker.Card(Poker.RANK.Ten, Poker.SUIT.Hearts)},
            {"345646D6AF22D03D38E73187917A6293", new Poker.Card(Poker.RANK.Jack, Poker.SUIT.Hearts)},
            {"3BA958908BA8E88BE828C5BE93AEDA7B", new Poker.Card(Poker.RANK.Queen, Poker.SUIT.Hearts)},
            {"28F5A313743DA987F2EAFAA1ECBEB895", new Poker.Card(Poker.RANK.King, Poker.SUIT.Hearts)},

            // diamonds
            {"2B01107BDB6E63CA2DBA48CF4474FBEC", new Poker.Card(Poker.RANK.Ace, Poker.SUIT.Diamonds)},
            {"C30652067D72807E0EF485C97DAA20DE", new Poker.Card(Poker.RANK.Two, Poker.SUIT.Diamonds)},
            {"837FA3112F61FA770798AE832169AB22", new Poker.Card(Poker.RANK.Three, Poker.SUIT.Diamonds)},
            {"85702C3780C68448BE791EADFF44372F", new Poker.Card(Poker.RANK.Four, Poker.SUIT.Diamonds)},
            {"3856025F3BE46F6C2B1D52397661930E", new Poker.Card(Poker.RANK.Five, Poker.SUIT.Diamonds)},
            {"E71C6CABE7770910EC1FCEBC9C76E649", new Poker.Card(Poker.RANK.Six, Poker.SUIT.Diamonds)},
            {"7464061E0A68E9EBC03BAA7CA8D129CE", new Poker.Card(Poker.RANK.Seven, Poker.SUIT.Diamonds)},
            {"7AEBF6B0E6E6899E8EFE3857630A0FDD", new Poker.Card(Poker.RANK.Eight, Poker.SUIT.Diamonds)},
            {"6F0562CBD770714731713B2516255EBD", new Poker.Card(Poker.RANK.Nine, Poker.SUIT.Diamonds)},
            {"B32BA656291593F87648AF8F83C55042", new Poker.Card(Poker.RANK.Ten, Poker.SUIT.Diamonds)},
            {"FCE6ECB9726359B04903B698A9610920", new Poker.Card(Poker.RANK.Jack, Poker.SUIT.Diamonds)},
            {"CCA9C05ECE06CB93D80979D61BD70DA7", new Poker.Card(Poker.RANK.Queen, Poker.SUIT.Diamonds)},
            {"2567D49EEEB41C997E0F0571285F3460", new Poker.Card(Poker.RANK.King, Poker.SUIT.Diamonds)},

            // spades
            {"F5F8DAD3C3E76C26B0568A40973D234E", new Poker.Card(Poker.RANK.Ace, Poker.SUIT.Spades)},
            {"6EC806B6419FAF1D88E51030C9C80BA7", new Poker.Card(Poker.RANK.Two, Poker.SUIT.Spades)},
            {"B5346F38E3DF26A084BB8FE3CA46B988", new Poker.Card(Poker.RANK.Three, Poker.SUIT.Spades)},
            {"0BADA99FF8646CE133439EAC5392DD9B", new Poker.Card(Poker.RANK.Four, Poker.SUIT.Spades)},
            {"959A2F0118457E48D6904FB927C4A517", new Poker.Card(Poker.RANK.Five, Poker.SUIT.Spades)},
            {"8F929F54569FBA59F44E845A25F9602D", new Poker.Card(Poker.RANK.Six, Poker.SUIT.Spades)},
            {"5857C5538CEF13D19B8B62CBD1AFF09A", new Poker.Card(Poker.RANK.Seven, Poker.SUIT.Spades)},
            {"226F003869FB1B3F5D6CDF49868BAC71", new Poker.Card(Poker.RANK.Eight, Poker.SUIT.Spades)},
            {"57EA409CA3F17670208CBAE79742C194", new Poker.Card(Poker.RANK.Nine, Poker.SUIT.Spades)},
            {"9D35437A9A7492547DD973690F35FDDF", new Poker.Card(Poker.RANK.Ten, Poker.SUIT.Spades)},
            {"89ECB982B422FE5089B5214B912C81F5", new Poker.Card(Poker.RANK.Jack, Poker.SUIT.Spades)},
            {"E3A5BA5D778FD4A94100DCD79F1527C2", new Poker.Card(Poker.RANK.Queen, Poker.SUIT.Spades)},
            {"C38006D4A2A03C1CB6FF1B5A3C06FC8A", new Poker.Card(Poker.RANK.King, Poker.SUIT.Spades)}

        };

        #endregion

        public static Poker.Card[] Md5CompareTableCards(Bitmap windowImage)
        {
            List<Poker.Card> cards = new List<Poker.Card>();

            Bitmap[] tableCards = CropTableCards(windowImage);

            foreach (var card in tableCards)
            {
                // make hash value out of bitmap
                string hash = ImageTools.ImageToMd5(card);

                //card.Save("cards\\" + hash + ".png", System.Drawing.Imaging.ImageFormat.Png);

                Poker.Card foundCard;

                if (cardsImageHashTable.TryGetValue(hash, out foundCard))
                {
                    // success
                }
                else
                {
                    // tesseract method here? meaby come back later...

                    foundCard = new Poker.Card { Rank = Poker.RANK.None, Suit = Poker.SUIT.None };
                }

                cards.Add(foundCard);
            }

            return cards.ToArray();
        }

    }
}
