﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


using PInvoke; // windows native calls

namespace ImageToText
{
    class VirtualBox
    {
        IntPtr handle = IntPtr.Zero;

        //const string WINDOWCAPTION = "Windows 7 [Running] - Oracle VM VirtualBox";
        //const string WINDOWCLASS = "QWidget";

        public VirtualBox()
        {
            //IntPtr result = FindWindow(WINDOWCLASS, WINDOWCAPTION);

            IntPtr result = IntPtr.Zero;
            foreach (Process pList in Process.GetProcesses())
            {
                if (pList.MainWindowTitle.Contains("[Running] - Oracle VM VirtualBox"))
                {
                    result = pList.MainWindowHandle;
                }
            }

            if (result != IntPtr.Zero)
            {
                handle = result;

                User32.SetForegroundWindow(handle);
                User32.SendMessage(handle, User32.WM_SYSCOMMAND, User32.SC_RESTORE, 0);
            }
            else
            {
                // terminate program...
                throw new ApplicationException("Virtual windows is not running. Start virtual windows and try again.");
            }
        }

        public Bitmap TakeScreenshot()
        {
            User32.ShowWindow(handle, User32.SW_RESTORE);

            Thread.Sleep(20);

            // get te hDC of the target window
            IntPtr hdcSrc = User32.GetWindowDC(handle);

            //// get the size
            RECT windowRect = new RECT();
            User32.GetWindowRect(handle, ref windowRect);

            // get client size
            RECT clientRect = new RECT();
            User32.GetClientRect(handle, ref clientRect);

            Point point = new Point();
            User32.ClientToScreen(handle, ref point);

            int width = clientRect.right - clientRect.left;
            int height = clientRect.bottom - clientRect.top;

            // create a device context we can copy to
            IntPtr hdcDest = Gdi32.CreateCompatibleDC(hdcSrc);
            // create a bitmap we can copy it to,
            // using GetDeviceCaps to get the width/height
            IntPtr hBitmap = Gdi32.CreateCompatibleBitmap(hdcSrc, width, height);
            // select the bitmap object
            IntPtr hOld = Gdi32.SelectObject(hdcDest, hBitmap);
            // bitblt over
            Gdi32.BitBlt(hdcDest, 0, 0, width, height, hdcSrc, point.X - windowRect.left, point.Y - windowRect.top, Gdi32.SRCCOPY);
            // restore selection
            Gdi32.SelectObject(hdcDest, hOld);
            // clean up
            Gdi32.DeleteDC(hdcDest);
            User32.ReleaseDC(handle, hdcSrc);
            // get a .NET image object for it
            Image img = Image.FromHbitmap(hBitmap);
            // free up the Bitmap object
            Gdi32.DeleteObject(hBitmap);

            return new Bitmap(img).Clone(new Rectangle(0, 0, img.Width, img.Height), System.Drawing.Imaging.PixelFormat.Format32bppArgb);
        }

    }
}
