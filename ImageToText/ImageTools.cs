﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Security.Cryptography;
using System.Runtime.InteropServices;

namespace ImageToText
{
    class ImageTools
    {
        public static bool WithinTolerance(Color c1, Color c2, int tolerance)
        {
            return Math.Abs(c1.R - c2.R) < tolerance &&
                   Math.Abs(c1.G - c2.G) < tolerance &&
                   Math.Abs(c1.B - c2.B) < tolerance;
        }

        public static bool IsSameColor(Color first, Color second)
        {
            if (first.R.Equals(second.R) && first.G.Equals(second.G) && first.B.Equals(second.B))
                return true;
            else
                return false;
        }

        public static Bitmap MakeBlackAndWhite(Bitmap processedBitmap)
        {
            unsafe
            {
                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);

                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

                Parallel.For(0, heightInPixels, y =>
                {
                    byte* currentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        if (oldRed < 140 || oldGreen < 140 || oldBlue < 140)
                            currentLine[x] = currentLine[x + 1] = currentLine[x + 2] = 0;
                        else
                            currentLine[x] = currentLine[x + 1] = currentLine[x + 2] = 255;
                    }
                });
                processedBitmap.UnlockBits(bitmapData);
            }

            return processedBitmap;
        }

        /// <summary>
        /// Get every pixel color from image... returns Color[y][x]
        /// </summary>
        /// <param name="processedBitmap"></param>
        /// <returns>color[y][x]</returns>
        public static Color[][] ProcessImage(Bitmap processedBitmap)
        {
            Color[][] colorMap = new Color[processedBitmap.Height][];


            unsafe
            {
                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadOnly, processedBitmap.PixelFormat);
                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* ptrFirstPixel = (byte*)bitmapData.Scan0;

                int wC; // width pixel counter

                for (int y = 0; y < heightInPixels; y++)
                {
                    byte* currentLine = ptrFirstPixel + (y * bitmapData.Stride);

                    wC = 0;

                    colorMap[y] = new Color[processedBitmap.Width];

                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        //int oldBlue = currentLine[x]; // blue
                        //int oldGreen = currentLine[x + 1]; // green
                        //int oldRed = currentLine[x + 2]; // red

                        colorMap[y][wC++] = Color.FromArgb(currentLine[x + 3], currentLine[x + 2], currentLine[x + 1], currentLine[x]);
                    }
                }
                processedBitmap.UnlockBits(bitmapData);
            }

            return colorMap;
        }

        public static Bitmap MakeGrayscale3(Bitmap original)
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            //create the grayscale ColorMatrix
            ColorMatrix colorMatrix = new ColorMatrix(
               new float[][] 
      {
         new float[] {.3f, .3f, .3f, 0, 0},
         new float[] {.59f, .59f, .59f, 0, 0},
         new float[] {.11f, .11f, .11f, 0, 0},
         new float[] {0, 0, 0, 1, 0},
         new float[] {0, 0, 0, 0, 1}
      });

            //create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            //set the color matrix attribute
            attributes.SetColorMatrix(colorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();
            return newBitmap;
        }

        public static Bitmap ScaleImage(Bitmap original, int factor)
        {
            Bitmap scaled = new Bitmap(original.Width * factor, original.Height * factor);

            using (Graphics graphics = Graphics.FromImage(scaled))
            {
                graphics.DrawImage(original, new Rectangle(0, 0, scaled.Width, scaled.Height));
            }

            return scaled;
        }

        public static string ImageToMd5(Bitmap picture)
        {
            byte[] hash;

            unsafe
            {
                BitmapData bitmapData = picture.LockBits(new Rectangle(0, 0, picture.Width, picture.Height), ImageLockMode.ReadOnly, picture.PixelFormat);

                byte[] rawImageData = new byte[picture.Height * Math.Abs(bitmapData.Stride)];

                Marshal.Copy(bitmapData.Scan0, rawImageData, 0, rawImageData.Length);
                
                picture.UnlockBits(bitmapData);

                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                hash = md5.ComputeHash(rawImageData);
            }

            return BitConverter.ToString(hash, 0).Replace("-", "");
        }

        public static Bitmap SharpenImage(Bitmap processedBitmap)
        {
            unsafe
            {
                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);

                int bytesPerPixel = System.Drawing.Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;
                byte* PtrFirstPixel = (byte*)bitmapData.Scan0;

                Parallel.For(0, heightInPixels, y =>
                {
                    byte* currentLine = PtrFirstPixel + (y * bitmapData.Stride);
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        // club
                        if (oldGreen - oldBlue > 50 && oldGreen - oldRed > 50)
                        {
                            // blue             green                       red
                            currentLine[x] = 0; currentLine[x + 1] = 255; currentLine[x + 2] = 0; // green
                        }
                        // heart
                        else if (oldRed - oldBlue > 50 && oldRed - oldGreen > 50)
                        {
                            currentLine[x] = 0; currentLine[x + 1] = 0; currentLine[x + 2] = 255; // red
                        }
                        // spade
                        else if (oldBlue == oldGreen && oldGreen == oldRed && oldRed < 128)
                        {
                            currentLine[x] = currentLine[x + 1] = currentLine[x + 2] = 0; // black
                        }
                        // diamond
                        else if (oldBlue - oldRed > 50 && oldBlue - oldGreen > 50)
                        {
                            currentLine[x] = 255; currentLine[x + 1] = 0; currentLine[x + 2] = 0; // blue
                        }

                        else
                        {
                            currentLine[x] = currentLine[x + 1] = currentLine[x + 2] = 255; // white
                        }
                    }
                });
                processedBitmap.UnlockBits(bitmapData);
            }

            return processedBitmap;
        }
    }

}
