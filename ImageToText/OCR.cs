﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tesseract;

namespace ImageToText
{
    class OCR
    {
        public static string GetText(Bitmap picture, int scale, bool onlyDigits = false)
        {
            string text = "";

            // pre-prosess image
            picture.Save("debug_ocr_original.png", System.Drawing.Imaging.ImageFormat.Png);

            picture = ImageTools.ScaleImage(picture, scale); // scale X times larger
            picture.Save("debug_ocr_scaled.png", System.Drawing.Imaging.ImageFormat.Png);
            picture = ImageTools.MakeGrayscale3(picture); // make gray picture
            picture.Save("debug_ocr_gray.png", System.Drawing.Imaging.ImageFormat.Png);
            picture = ImageTools.MakeBlackAndWhite(picture); // "sharpen"
            picture.Save("debug_ocr_blackwhite.png", System.Drawing.Imaging.ImageFormat.Png);

            // extract text

            using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
            {
                if (onlyDigits)
                    engine.SetVariable("tessedit_char_whitelist", "0123456789,.");

                engine.DefaultPageSegMode = PageSegMode.SingleLine;

                using (var pix = PixConverter.ToPix(picture))
                {
                    using (var page = engine.Process(pix))
                    {
                        text = page.GetText();
                    }
                }
            }

            return text;
        }

        public static string GetCharacter(Bitmap picture, int scale)
        {
            string text = "";

            // pre-prosess image
            picture.Save("debug_ocr_original.png", System.Drawing.Imaging.ImageFormat.Png);

            picture = ImageTools.ScaleImage(picture, scale); // scale X times larger
            picture.Save("debug_ocr_scaled.png", System.Drawing.Imaging.ImageFormat.Png);
            //picture = ImageTools.MakeGrayscale3(picture); // make gray picture
            picture.Save("debug_ocr_gray.png", System.Drawing.Imaging.ImageFormat.Png);
            picture = ImageTools.MakeBlackAndWhite(picture); // "sharpen"
            picture.Save("debug_ocr_blackwhite.png", System.Drawing.Imaging.ImageFormat.Png);

            // extract text

            using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
            {
                engine.SetVariable("tessedit_char_whitelist", "023456789JQKA");
                // engine.DefaultPageSegMode = PageSegMode.SingleChar;

                using (var pix = PixConverter.ToPix(picture))
                {
                    using (var page = engine.Process(pix, PageSegMode.SingleChar))
                    {
                        text = page.GetText().Trim();
                    }
                }
            }

            return text;
        }
    }
}
