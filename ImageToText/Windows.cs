﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageToText
{
    class Windows
    {
        public static Bitmap[] FindWindows(Bitmap desktopImage)
        {
            Color[][] map = ImageTools.ProcessImage(desktopImage);

            List<Bitmap> windows = new List<Bitmap>();

            // window border color
            // focus
            Color windowBorder = Color.FromArgb(52, 52, 52);
            Color windowBorder2 = Color.FromArgb(0, 0, 0);

            // no focus
            Color windowBorder3 = Color.FromArgb(77, 77, 77); // +-1 window that has no 'focus'

            // loop
            int width = map[0].Length;
            int height = map.Length;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    // check if out of boundries.. must see every window..
                    if (x + 480 >= width)
                        break;

                    // window that has a focus

                    // top part of the window
                    if (map[y][x] == windowBorder && map[y][x + 44] == windowBorder && map[y][x + 219] == windowBorder && map[y][x + 480] == windowBorder)
                    {
                        // sides
                        if (map[y + 36][x - 4] == windowBorder2 && map[y + 36][x + 486] == windowBorder2)
                        {
                            // bottom
                            if (map[y + 364][x] == windowBorder2 && map[y + 364][x + 486] == windowBorder2)
                            {
                                windows.Add(desktopImage.Clone(new Rectangle(x - 4, y, 491, 365), desktopImage.PixelFormat));
                            }
                        }
                    }
                    else
                    {

                        // no focus window

                        if (ImageTools.WithinTolerance(map[y][x], windowBorder3, 2) && ImageTools.WithinTolerance(map[y][x + 44], windowBorder3, 2) && ImageTools.WithinTolerance(map[y][x + 219], windowBorder3, 2) && ImageTools.WithinTolerance(map[y][x + 480], windowBorder3, 2))
                        {
                            // sides
                            if (ImageTools.WithinTolerance(map[y + 36][x - 4], windowBorder3, 2) && ImageTools.WithinTolerance(map[y + 36][x + 486], windowBorder3, 2))
                            {
                                // bottom
                                if (ImageTools.WithinTolerance(map[y + 364][x], windowBorder3, 2) && ImageTools.WithinTolerance(map[y + 364][x + 486], windowBorder3, 2))
                                {
                                    windows.Add(desktopImage.Clone(new Rectangle(x - 4, y, 491, 365), desktopImage.PixelFormat));
                                }
                            }
                        }
                    }

                }

                // check if y coords out of boundries
                if (y + 364 >= height)
                    break;
            }

            return windows.ToArray();
        }
    }
}
