﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using PInvoke;

namespace ImageToText.Mouse
{
    class Mouse
    {
        public static void LiikutaHiirtä(int maaliX, int maaliY)
        {
            POINT lpPoint;
            User32.GetCursorPos(out lpPoint);

            int alkuX = lpPoint.X;
            int alkuY = lpPoint.Y;

            // jakaa liikkeen sataan pienempään liikkeeseen 10ms välein
            double kertaLiikeX = (maaliX - alkuX) / 100.0;
            double kertaLiikeY = (maaliY - alkuY) / 100.0;
            for (int i = 1; i <= 100; i++)
            {
                int liikuX = (int)Math.Round(kertaLiikeX * i, 0);
                int liikuY = (int)Math.Round(kertaLiikeY * i, 0);
                Thread.Sleep(10);

                User32.SetCursorPos((alkuX + liikuX), (alkuY + liikuY));
            }
        }

        public static void PainaHiirtä()
        {
            User32.mouse_event((uint)MouseEventFlags.LEFTDOWN | (uint)MouseEventFlags.LEFTUP, 0, 0, 0, 0);
        }

        public static void MoveMouse(Rectangle target, int speed)
        {
            Random random = new Random();
            //binomijakaumalla saadaan useimmin keskimmäinen
            int finalX = random.Next(target.X, target.X + target.Width + 1);
            finalX += random.Next(target.X, target.X + target.Width + 1);
            finalX = finalX / 2;

            int finalY = random.Next(target.Y, target.Y + target.Height + 1);
            finalY += random.Next(target.Y, target.Y + target.Height + 1);
            finalY = finalY / 2;

            //API.SetCursorPos(finalX, finalY);
            
            POINT lpPoint;
            User32.GetCursorPos(out lpPoint);

            int alkuX = lpPoint.X;
            int alkuY = lpPoint.Y;

            // jakaa liikkeen 125 pienempään liikkeeseen 8ms välein eli 125hz(hiiren normi päivitysnopeus)
            double kertaLiikeX = (finalX - alkuX) / (speed / 8.0);
            double kertaLiikeY = (finalY - alkuY) / (speed / 8.0);
            for (int i = 1; i <= speed / 8.0; i++)
            {
                int liikuX = (int)Math.Round(kertaLiikeX * i, 0);
                int liikuY = (int)Math.Round(kertaLiikeY * i, 0);
                Thread.Sleep(8);

                User32.SetCursorPos((alkuX + liikuX), (alkuY + liikuY));
            }
        }

        public static void TestMouse()
        {
            //Random rnd = new Random();
            Rectangle targetArea = new Rectangle(0, 0, 1920, 1080);

            for (int i = 0; i < 30; i++)
            {
                MoveMouse(targetArea, 100);
                Thread.Sleep(1);
            }
        }
    }
}
