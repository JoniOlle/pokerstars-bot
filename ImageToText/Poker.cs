﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageToText
{
    class Poker
    {
        // cards, decks, rules, styles, limits, terms...
        public enum STREET { None, Flop, Turn, River }
        public enum SUIT { None = -1, Diamonds = 1, Hearts = 2, Clubs = 0, Spades = 3 }
        public enum RANK
        {
            None, Ace, Two, Three, Four, Five, Six, Seven, Eight,
            Nine, Ten, Jack, Queen, King
        }

        public struct Card
        {
            public SUIT Suit;
            public RANK Rank;

            public Card(RANK rank, SUIT suit)
            {
                this.Rank = rank;
                this.Suit = suit;
            }
        }

        public class Holecards
        {
            public Card holeCardOne;
            public Card holeCardTwo;

            public Holecards(Card holeCardOne, Card holeCardTwo)
            {
                this.holeCardOne = holeCardOne;
                this.holeCardTwo = holeCardTwo;
            }
        }

        public class Board
        {
            private Card[] flop;
            private Card turn;
            private Card river;

            public Board(Card flopCardOne, Card flopCardTwo, Card flopCardThree)
            {
                this.flop = new Card[] { flopCardOne, flopCardTwo, flopCardThree };
            }

            public void TurnDealt(Card turnCard)
            {
                this.turn = turnCard;
            }

            public void RiverDealt(Card riverCard)
            {
                this.river = riverCard;
            }

            /// <summary>
            /// Combine player cards and board cards and make best hand...
            /// </summary>
            /// <param name="PlayerPocketCards"></param>
            /// <returns>Complete hand cards</returns>
            public Card[] HandRanking(Holecards PlayerPocketCards)
            {
                return new Card[5];
            }
        }

        public struct Table
        {
            public string Name { get; set; }
            public float PotSize { get; set; }
            public long HandId { get; set; }

            public override string ToString()
            {
                return string.Format("[TABLEINFO] name = {0}, handId = {1} potsize = {2}", this.Name, this.HandId, this.PotSize);
            }
        }
    }
}
