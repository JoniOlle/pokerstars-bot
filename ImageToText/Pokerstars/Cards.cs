﻿using System;
using System.Drawing;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageToText.Pokerstars
{
    class Cards
    {
        // format "club\\EA55ED4C319531AFE05DB54A70686C38.png" == Ace of clubs
        const string folder = "cards\\pokerstars\\";

        // order clubs, diamonds, hearts, spades
        Color[][][][] allCards = new Color[4][][][];
        string[] cardNames = new string[13 * 4];

        public Cards()
        {
            // init
            allCards[0] = new Color[13][][]; // club
            allCards[1] = new Color[13][][]; // diamond
            allCards[2] = new Color[13][][]; // heart
            allCards[3] = new Color[13][][]; // spade

            // load known poker card images

            for (int sIndex = 0; sIndex < 4; sIndex++)
            {
                string[] filePaths;

                if (sIndex == 0)
                    filePaths = Directory.GetFiles(folder + "club\\", "*.png");
                else if (sIndex == 1)
                    filePaths = Directory.GetFiles(folder + "diamond\\", "*.png");
                else if (sIndex == 2)
                    filePaths = Directory.GetFiles(folder + "heart\\", "*.png");
                else
                    filePaths = Directory.GetFiles(folder + "spade\\", "*.png");

                for(int rIndex = 0; rIndex < 13; rIndex++)
                {
                    string filePath = filePaths[rIndex];

                    allCards[sIndex][rIndex] = ImageTools.ProcessImage(new Bitmap(filePath));

                    cardNames[sIndex * 13 + rIndex] = filePath.Split('\\').Last();
                }
            }
            
        }

        public void IdentifyTableCards(Bitmap windowImage, out Poker.Card[] identifiedCards, out float[] confidence)
        {
            // chop tablecards out of window image
            Bitmap[] tableCards = ScanTableImage.CropTableCards(windowImage);

            List<Poker.Card> cards = new List<Poker.Card>(5);
            List<float> confidences = new List<float>(5);

            Color white = Color.FromArgb(255,255,255);

            foreach (var cardImage in tableCards)
            {
                Poker.SUIT suit = ScanTableImage.DetermineSuit(cardImage);

                int bestMatch = -1;
                float bestConf = -1;

                for (int rank = 0; rank < 13; rank++)
                {
                    int sameColors = 0;
                    int possipleColors = 0;
                    float conf;

                    // COMPARE PIXELS, GET BEST MATCH
                    Color[][] memPixels = allCards[(int)suit][rank];
                    Color[][] ocrPixels = ImageTools.ProcessImage(cardImage);

                    for (int y = 0; y < cardImage.Height; y++)
                    {
                        for (int x = 0; x < cardImage.Width; x++)
                        {
                            Color color1 = memPixels[y][x], color2 = ocrPixels[y][x];

                            // skip all white pixels
                            if (color1 == white)
                            {
                                continue;
                            }

                            possipleColors++;

                            if (ImageTools.IsSameColor(color1, color2))
                            {
                                sameColors++;
                            }
                        }
                    }

                    // calculate confidence

                    conf = (float)sameColors / (float)possipleColors;

                    if (bestConf < conf)
                    {
                        bestMatch = rank;
                        bestConf = conf;
                    }
                }

                // ADD TO LIST

                string hashName = this.cardNames[(int)suit * 13 + bestMatch].Split('.')[0];

                Poker.Card foundCard;

                if (ScanTableImage.cardsImageHashTable.TryGetValue(hashName, out foundCard))
                {
                    // success
                }
                else
                {
                    // tesseract method here? meaby come back later...

                    foundCard = new Poker.Card { Rank = Poker.RANK.None, Suit = Poker.SUIT.None };
                }

                cards.Add(foundCard);
                confidences.Add(bestConf);
            }

            // set values
            identifiedCards = cards.ToArray();
            confidence = confidences.ToArray();
        }
    }
}
