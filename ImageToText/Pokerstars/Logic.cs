﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageToText.Pokerstars
{
    class Logic
    {
        private VirtualBox virtualBox;
        private List<Poker.Table> pokerTables;

        public Logic(VirtualBox virtualBox)
        {
            // initialize

            this.virtualBox = virtualBox;
            this.pokerTables = new List<Poker.Table>();
        }

        public void Loop()
        {
            // load cards..
            Cards PSCards = new Cards();

            while (true)
            {
                foreach (var windowImage in Windows.FindWindows(this.virtualBox.TakeScreenshot()))
                {
                    // identify window
                    string windowName = OCR.GetText(ScanTableImage.CropTableName(windowImage), 6);

                    windowImage.Save("debug_window.png", System.Drawing.Imaging.ImageFormat.Png);

                    Poker.Table table = new Poker.Table();
                    table.Name = windowName.Split('-')[0].TrimEnd();
                    table.PotSize = ScanTableImage.GetPotSize(windowImage);
                    table.HandId = ScanTableImage.CropTableHandId(windowImage);

                    //ScanTableImage.CropTableCards(windowImage);

                    //var cards = ScanTableImage.TesseractTableCards(windowImage);

                    //                    foreach(var card in cards)
                    //                      Console.WriteLine(card.Rank + "\t" + card.Suit);

                    // var cards = ScanTableImage.Md5CompareTableCards(windowImage);

                    Poker.Card[] cards;
                    float[] confidence;

                    PSCards.IdentifyTableCards(windowImage, out cards, out confidence);

                    int i = 0;

                    foreach (var card in cards)
                        Console.WriteLine(card.Rank + "\t" + card.Suit + ", \t Confidence: " + confidence[i++]);

                    //pokerTables.Add(table);


                    Console.WriteLine(table.ToString());
                }
            }
        }
    }
}
