﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageToText
{
    class Program
    {
        static void Main(string[] args)
        {
            VirtualBox vb = new VirtualBox();
            // vb.TakeScreenshot().Save("debug_virtual_desktop.png", System.Drawing.Imaging.ImageFormat.Png);

            Pokerstars.Logic stars = new Pokerstars.Logic(vb);
            stars.Loop();

            Console.WriteLine("Press any key to Exit..");
            Console.ReadKey();
        }
    }
}
